#include <iostream>
#include <filesystem>
#include <string>
#include <optional>

#include <eoblib.h>
#include <eob_data_printer.h>

int main(int argc, char** argv)
{
    std::cout << "Hack the Beholder demo\n\n";

    std::string fileName{"darkmoon-qsp.sav"};
    std::string fullPath = EobLibSettings::data_dir_path + fileName;
    std::experimental::filesystem::path filePath{fullPath};

    EobLib::EobData d{filePath, EobLib::Version::darkmoon};
    std::cout << "Input file: " << d.get_full_path() << "\n";
    std::cout << "Save name: " << d.get_save_name() << "\n";

    if (!d.parse_file())
    {
        std::cout << "Unable to parse file " << d.get_full_path() << "\n";
        return 1;
    }

    EobLib::EobDataPrinter printer{d};
    printer.show_characters();

    return 0;
}