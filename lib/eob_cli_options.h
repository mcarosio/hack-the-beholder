#ifndef EOB_CLI_OPTIONS_H
#define EOB_CLI_OPTIONS_H

#include <boost/program_options.hpp>

#include <fstream>
#include <sstream>
#include <optional>

namespace EobLib
{
    struct CliOptions
    {
        static const char* help;
        static const char* path;
        static const char* character;
        static const char* version;
        static const char* show;
        static const char* set_strength;
        static const char* set_dexterity;
        static const char* set_constitution;
        static const char* set_intelligence;
        static const char* set_wisdom;
        static const char* set_charisma;
        static const char* set_hitpoints;
        static const char* set_max_hitpoints;
        static const char* set_experience;
        static const char* set_level;
        static const char* set_alignment;
        static const char* set_race;
        static const char* set_sex;
        static const char* set_ac;
        static const char* set_class_id;
        static const char* set_food_percentage;
        static const char* set_portrait;
    };

    class EobCliOptions
    {
    public:
        EobCliOptions(int argc, char** argv);

        bool has_option(const char* opt) const
        {
            return _vm.count(opt) != 0;
        }
        const std::string get_help() const
        {
            std::stringstream ss{};
            ss << _desc;
            return ss.str();
        }
        friend std::ofstream& operator<<(std::ofstream& out, const EobCliOptions& opt)
        {
            out << opt.get_help();
            return out;
        }

        template<typename _Type>
        std::optional<_Type> get_option(const char* opt, const std::optional<_Type>& defaultValue = std::nullopt) const
        {
            if (has_option(opt))
            {
                return _vm[opt].as<_Type>();
            }
            if (defaultValue)
            {
                return defaultValue;
            }
            return std::nullopt;
        }

    private:
        boost::program_options::options_description _desc;
        boost::program_options::variables_map _vm;
    
        void add_options();
    };
} // namespace EobLib

#endif //EOB_CLI_OPTIONS_H