#include <eoblib.h>

#include <sstream>
#include <exception>
#include <iterator>
#include <optional>
#include <vector>
#include <iomanip>
#include <cstring>

namespace EobLib
{
    /* ********************************
    Static data
    ******************************** */
    std::map<Class, std::string_view> EobData::_classDescr =
    {
        {Class::fighter, Description::Class::fighter},
        {Class::ranger, Description::Class::ranger},
        {Class::paladin, Description::Class::paladin},
        {Class::mage, Description::Class::mage},
        {Class::cleric, Description::Class::cleric},
        {Class::thief, Description::Class::thief},
        {Class::fighter_cleric, Description::Class::fighter_cleric},
        {Class::fighter_thief, Description::Class::fighter_thief},
        {Class::fighter_mage, Description::Class::fighter_mage},
        {Class::fighter_mage_thief, Description::Class::fighter_mage_thief},
        {Class::thief_mage, Description::Class::thief_mage},
        {Class::cleric_mage, Description::Class::cleric_mage},
        {Class::fighter_cleric_mage, Description::Class::fighter_cleric_mage},
        {Class::ranger_cleric, Description::Class::ranger_cleric},
        {Class::cleric_mage, Description::Class::cleric_mage}
    };
      
    std::map<Class, std::string_view> EobData::_classAcronym =
    {
        {Class::fighter, Description::ClassAcronym::fighter},
        {Class::ranger, Description::ClassAcronym::ranger},
        {Class::paladin, Description::ClassAcronym::paladin},
        {Class::mage, Description::ClassAcronym::mage},
        {Class::cleric, Description::ClassAcronym::cleric},
        {Class::thief, Description::ClassAcronym::thief},
        {Class::fighter_cleric, Description::ClassAcronym::fighter_cleric},
        {Class::fighter_thief, Description::ClassAcronym::fighter_thief},
        {Class::fighter_mage, Description::ClassAcronym::fighter_mage},
        {Class::fighter_mage_thief, Description::ClassAcronym::fighter_mage_thief},
        {Class::thief_mage, Description::ClassAcronym::thief_mage},
        {Class::cleric_mage, Description::ClassAcronym::cleric_mage},
        {Class::fighter_cleric_mage, Description::ClassAcronym::fighter_cleric_mage},
        {Class::ranger_cleric, Description::ClassAcronym::ranger_cleric},
        {Class::cleric_mage, Description::ClassAcronym::cleric_mage}
    };
         
    std::map<Race, std::string_view> EobData::_raceDescr =
    {
        {Race::human, Description::Race::human},
        {Race::elf, Description::Race::elf},
        {Race::half_elf,Description::Race::half_elf},
        {Race::dwarf, Description::Race::dwarf},
        {Race::gnome, Description::Race::gnome},
        {Race::halfling, Description::Race::halfling}
    };
         
    std::map<Sex, std::string_view> EobData::_sexDescr =
    {
        {Sex::male, Description::Sex::male},
        {Sex::female, Description::Sex::female},
    };
         
    std::map<Alignment, std::string_view> EobData::_alignDescr =
    {
        {Alignment::lawful_good, Description::Alignment::lawful_good},
        {Alignment::lawful_neutral, Description::Alignment::lawful_neutral},
        {Alignment::lawful_evil, Description::Alignment::lawful_evil},
        {Alignment::neutral_good, Description::Alignment::neutral_good},
        {Alignment::true_neutral, Description::Alignment::neutral},
        {Alignment::neutral_evil, Description::Alignment::neutral_evil},
        {Alignment::chaotic_good, Description::Alignment::chaotic_good},
        {Alignment::chaotic_neutral, Description::Alignment::chaotic_neutral},
        {Alignment::chaotic_evil, Description::Alignment::chaotic_evil},
    };
         
    std::map<Alignment, std::string_view> EobData::_alignAcronym =
    {
        {Alignment::lawful_good, Description::AlignAcronym::lawful_good},
        {Alignment::lawful_neutral, Description::AlignAcronym::lawful_neutral},
        {Alignment::lawful_evil, Description::AlignAcronym::lawful_evil},
        {Alignment::neutral_good, Description::AlignAcronym::neutral_good},
        {Alignment::true_neutral, Description::AlignAcronym::neutral},
        {Alignment::neutral_evil, Description::AlignAcronym::neutral_evil},
        {Alignment::chaotic_good, Description::AlignAcronym::chaotic_good},
        {Alignment::chaotic_neutral, Description::AlignAcronym::chaotic_neutral},
        {Alignment::chaotic_evil, Description::AlignAcronym::chaotic_evil},
    };
         
    std::map<Skill, std::string_view> EobData::_skillDescr =
    {
        {Skill::strength, Description::Skill::strength},
        {Skill::dexterity, Description::Skill::dexterity},
        {Skill::constitution, Description::Skill::constitution},
        {Skill::intelligence, Description::Skill::intelligence},
        {Skill::wisdom, Description::Skill::wisdom},
        {Skill::charisma, Description::Skill::charisma},
    };
         
    std::set<Class> EobData::_fighterClasses =
    {
        Class::fighter, Class::ranger, Class::paladin, Class::fighter_cleric,
        Class::fighter_thief, Class::fighter_mage, Class::fighter_mage_thief,
        Class::fighter_cleric_mage, Class::ranger_cleric,
    };    

    std::set<Class> EobData::_multiclasses =
    {
        Class::fighter_cleric, Class::fighter_thief, Class::fighter_mage,
        Class::fighter_mage_thief, Class::thief_mage, Class::cleric_thief,
        Class::fighter_cleric_mage, Class::ranger_cleric, Class::cleric_mage,
    };  

    std::set<Class> EobData::_doubleClass =
    {
        Class::fighter_cleric, Class::fighter_thief, Class::fighter_mage,
        Class::thief_mage, Class::cleric_thief, Class::ranger_cleric, Class::cleric_mage,
    };  

    std::set<Class> EobData::_tripleClass =
    {
        Class::fighter_mage_thief, Class::fighter_cleric_mage,
    };
    

    /* ********************************
    EobData
    ******************************** */
    EobData::EobData(const std::experimental::filesystem::path& filePath, const Version gameVersion)
        : _saveFile{filePath},
            _inStream{filePath.string(), std::ios::in | std::ios::binary},
            _fileSize{std::experimental::filesystem::file_size(_saveFile)},
            _gameVersion{gameVersion},
            _headerLength{(_gameVersion == Version::darkmoon) ? Const::darkmoon_header_length : 0}
    {
        if (!std::experimental::filesystem::exists(_saveFile))
        {
            std::stringstream ss;
            ss << "Save file " << _saveFile.string() << " does not exists";
            throw std::runtime_error(ss.str());
        }

        _inStream.unsetf(std::ios::skipws);
        _rawData.reserve(_fileSize);
    }

    bool EobData::parse_file()
    {
        _rawData.insert(_rawData.begin(),
               std::istream_iterator<uint8_t>(_inStream),
               std::istream_iterator<uint8_t>());
        return true;
    }

    void EobData::save_file(const std::experimental::filesystem::path& filePath)
    {
        std::ofstream outFile(filePath.string(), std::ios::out | std::ios::binary);
        outFile.write(reinterpret_cast<const char*>(_rawData.data()), _fileSize);
        outFile.flush();
        outFile.close();
    }

    auto EobData::get_save_name() const -> std::string
    {
        std::string name{"Unnamed"};
        if (_gameVersion == Version::darkmoon)
        {
            name = std::string(Const::darkmoon_header_length, '\0');
            for (size_t s=0; s<Const::darkmoon_header_length; ++s)
            {
                auto c = static_cast<char>(_rawData[s]);
                name[s] = c;
            }
        }
        return name;
    }

    void EobData::set_save_name(const std::string& saveName)
    {
        // if (_gameVersion == Version::Darkmoon)
        // {
        //     std::copy(
        //         _rawData.begin(),
        //         _rawData.begin() + Const::darkmoon_header_length,
        //         saveName.begin());
        // }
    }

    std::string EobData::get_full_path() const
    {
        return _saveFile.string();
    }

    auto EobData::has_exceptional_strength(const size_t characterIndex) const -> bool
    {
        auto cls = get_class_id(characterIndex);
        if (_fighterClasses.find(cls) != _fighterClasses.end())
        {
            auto str = get_strength(characterIndex);
            return str == 18;
        }
        return false;
    }

    auto EobData::is_double_class(const size_t characterIndex) const -> bool
    {
        auto cls = get_class_id(characterIndex);
        return _doubleClass.find(cls) != _doubleClass.end();
    }

    auto EobData::is_triple_class(const size_t characterIndex) const -> bool
    {
        auto cls = get_class_id(characterIndex);
        return _tripleClass.find(cls) != _tripleClass.end();
    }

    void EobData::set_active(const size_t characterIndex, const bool active)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.active == (active) ? 1 : 0;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.active == (active) ? 1 : 0;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    void EobData::set_name(const size_t characterIndex, const std::string& chrName)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            //std::copy(chr.name, chr.name + sizeof(chr.name), chrName.begin());
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            //std::copy(chr.name, chr.name + sizeof(chr.name), chrName.begin());
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::read_xps(const uint8_t bytes[]) -> uint32_t
    {
        uint32_t dword = bytes[0];
        for (int i=1; i<=3; ++i)
        {
            dword <<= 8;
            dword += bytes[i];
        }
        return
            ((dword>>24)&0x000000FF)
            | ((dword>>8)&0x0000FF00)
            | ((dword<<8)&0x00FF0000)
            | ((dword<<24)&0xFF000000);
    }

    auto EobData::xps_to_bytes(const uint32_t xps) -> std::vector<uint8_t>
    {
        std::vector<uint8_t> bytes;

        bytes.push_back(xps & 0x000000FF);
        bytes.push_back((xps>>8)&0x000000FF);
        bytes.push_back((xps>>16)&0x000000FF);
        bytes.push_back((xps>>24)&0x000000FF);

        return bytes;
    }

    auto EobData::is_valid_index(const size_t characterIndex) const -> bool
    {
        return characterIndex > Const::max_party_size-1;
    }

    auto EobData::is_active(const size_t characterIndex) const -> bool
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.active == 1;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.active == 1;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_name(const size_t characterIndex) const -> std::string
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return std::string(chr.name, strlen(chr.name));
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return std::string(chr.name, strlen(chr.name));
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_skill(const Skill& skl, const size_t characterIndex) const -> short
    {
        if (skl == Skill::strength)
        {
            return get_strength(characterIndex);
        }
        else if (skl == Skill::dexterity)
        {
            return get_dexterity(characterIndex);
        }
        else if (skl == Skill::constitution)
        {
            return get_constitution(characterIndex);
        }
        else if (skl == Skill::intelligence)
        {
            return get_intelligence(characterIndex);
        }
        else if (skl == Skill::wisdom)
        {
            return get_wisdom(characterIndex);
        }
        else //if (skl == Skill::charisma)
        {
            return get_charisma(characterIndex);
        }
    }

    auto EobData::get_strength(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.modifiedStrength;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.modifiedStrength;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_exc_strength(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.modifiedExceptionalStrength;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.modifiedExceptionalStrength;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_dexterity(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.modifiedDexterity;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.modifiedDexterity;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_constitution(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.modifiedConstitution;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.modifiedConstitution;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_intelligence(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.modifiedIntelligence;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.modifiedIntelligence;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_wisdom(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.modifiedWisdom;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.modifiedWisdom;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_charisma(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.modifiedCharisma;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.modifiedCharisma;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_base_strength(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.baseStrength;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.baseStrength;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_base_exc_strength(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.baseExceptionalStrength;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.baseExceptionalStrength;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_base_dexterity(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.baseDexterity;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.baseDexterity;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_base_constitution(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.baseConstitution;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.baseConstitution;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_base_intelligence(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.baseIntelligence;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.baseIntelligence;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_base_wisdom(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.baseWisdom;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.baseWisdom;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_base_charisma(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.baseCharisma;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.baseCharisma;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_hit_points(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.hitPoints;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.hitPoints;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    auto EobData::get_max_hit_points(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.maxHitPoints;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.maxHitPoints;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    auto EobData::get_armor_class(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.armorClass;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.armorClass;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    auto EobData::get_class_id(const size_t characterIndex) const -> Class
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return static_cast<Class>(chr.classId);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return static_cast<Class>(chr.classId);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    auto EobData::get_race(const size_t characterIndex) const -> Race
    {
        uint8_t raceId{};
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            raceId = chr.race;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            raceId = chr.race;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
        
        if (raceId == 0 || raceId == 1)
        {
            return Race::human;
        }
        else if (raceId == 2 || raceId == 3)
        {
            return Race::elf;
        }
        else if (raceId == 4 || raceId == 5)
        {
            return Race::half_elf;
        }
        else if (raceId == 6 || raceId == 7)
        {
            return Race::dwarf;
        }
        else if (raceId == 8 || raceId == 9)
        {
            return Race::gnome;
        }
        else if (raceId == 10 || raceId == 11)
        {
            return Race::halfling;
        }

        throw std::runtime_error("Invalid race ID");
    }
    
    auto EobData::get_sex(const size_t characterIndex) const -> Sex
    {
        uint8_t raceId{};
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            raceId = chr.race;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            raceId = chr.race;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
        
        return (raceId % 2 == 0) ? Sex::male : Sex::female;
    }
    
    auto EobData::get_alignment(const size_t characterIndex) const -> Alignment
    {
        uint8_t alignId{};
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            alignId = chr.alignment;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            alignId = chr.alignment;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
        
        if (alignId == 0)
        {
            return Alignment::lawful_good;
        }
        else if (alignId == 1)
        {
            return Alignment::neutral_good;
        }
        else if (alignId == 2)
        {
            return Alignment::chaotic_good;
        }
        else if (alignId == 3)
        {
            return Alignment::lawful_neutral;
        }
        else if (alignId == 4)
        {
            return Alignment::true_neutral;
        }
        else if (alignId == 5)
        {
            return Alignment::chaotic_neutral;
        }
        else if (alignId == 6)
        {
            return Alignment::lawful_evil;
        }
        else if (alignId == 7)
        {
            return Alignment::neutral_evil;
        }
        else if (alignId == 8)
        {
            return Alignment::chaotic_evil;
        }

        throw std::runtime_error("Invalid alignment ID");
    }
    
    auto EobData::get_food_percentage(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.foodPerc;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.foodPerc;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    auto EobData::get_portrait(const size_t characterIndex) const -> short
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            return chr.portrait;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            return chr.portrait;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    auto EobData::get_level(const size_t characterIndex) const -> Level
    {
        uint8_t level[3]{};

        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            level[0] = chr.level;
            level[1] = chr.levelMultiClass1;
            level[2] = chr.levelMultiClass2;
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            level[0] = chr.level;
            level[1] = chr.levelMultiClass1;
            level[2] = chr.levelMultiClass2;
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
        
        auto secondLevel = (is_double_class(characterIndex)) ? std::make_optional(level[1]) : std::nullopt;
        auto thirdLevel = (is_triple_class(characterIndex)) ? std::make_optional(level[2]) : std::nullopt;

        return std::make_tuple(level[0], secondLevel, thirdLevel);
    }
    
    auto EobData::get_experience(const size_t characterIndex) const -> XP
    {
        uint32_t xp[3]{};

        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            xp[0] = read_xps(chr.experience);
            xp[1] = read_xps(chr.experienceMultiClass1);
            xp[2] = read_xps(chr.experienceMultiClass2);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            xp[0] = read_xps(chr.experience);
            xp[1] = read_xps(chr.experienceMultiClass1);
            xp[2] = read_xps(chr.experienceMultiClass2);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
        
        auto secondLevel = (is_double_class(characterIndex)) ? std::make_optional(xp[1]) : std::nullopt;
        auto thirdLevel = (is_triple_class(characterIndex)) ? std::make_optional(xp[2]) : std::nullopt;

        return std::make_tuple(xp[0], secondLevel, thirdLevel);
    }
    
    const std::string_view EobData::get_class_description(const Class classId, const std::optional<std::string_view>& defaultValue)
    {
        if (_classDescr.find(classId) != _classDescr.end())
        {
            return _classDescr[classId];
        }
        if (defaultValue)
        {
            return defaultValue.value();
        }
        throw std::runtime_error("Unrecognised class ID");
    }

    const std::string_view EobData::get_sex_description(const Sex sexId, const std::optional<std::string_view>& defaultValue)
    {
        if (_sexDescr.find(sexId) != _sexDescr.end())
        {
            return _sexDescr[sexId];
        }
        if (defaultValue)
        {
            return defaultValue.value();
        }
        throw std::runtime_error("Unrecognised sex ID");
    }

    const std::string_view EobData::get_race_description(const Race raceId, const std::optional<std::string_view>& defaultValue)
    {
        if (_raceDescr.find(raceId) != _raceDescr.end())
        {
            return _raceDescr[raceId];
        }
        if (defaultValue)
        {
            return defaultValue.value();
        }
        throw std::runtime_error("Unrecognised race ID");
    }

    const std::string_view EobData::get_alignment_description(const Alignment alignId, const std::optional<std::string_view>& defaultValue)
    {
        if (_alignDescr.find(alignId) != _alignDescr.end())
        {
            return _alignDescr[alignId];
        }
        if (defaultValue)
        {
            return defaultValue.value();
        }
        throw std::runtime_error("Unrecognised alignment ID");
    }

    const std::string_view EobData::get_alignment_acronym(const Alignment alignId, const std::optional<std::string_view>& defaultValue)
    {
        if (_alignAcronym.find(alignId) != _alignAcronym.end())
        {
            return _alignAcronym[alignId];
        }
        if (defaultValue)
        {
            return defaultValue.value();
        }
        throw std::runtime_error("Unrecognised alignment ID");
    }

    const std::string_view EobData::get_class_acronym(const Class classId, const std::optional<std::string_view>& defaultValue)
    {
        if (_classAcronym.find(classId) != _classAcronym.end())
        {
            return _classAcronym[classId];
        }
        if (defaultValue)
        {
            return defaultValue.value();
        }
        throw std::runtime_error("Unrecognised class ID");
    }

    const std::string_view EobData::get_skill_description(const Skill skill, const std::optional<std::string_view>& defaultValue)
    {
        if (_skillDescr.find(skill) != _skillDescr.end())
        {
            return _skillDescr[skill];
        }
        if (defaultValue)
        {
            return defaultValue.value();
        }
        throw std::runtime_error("Unrecognised skill ID");
    }

    void EobData::set_strength(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.modifiedStrength = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.modifiedStrength = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

    void EobData::set_exc_strength(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.modifiedExceptionalStrength = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.modifiedExceptionalStrength = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_dexterity(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.modifiedDexterity = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.modifiedDexterity = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_constitution(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.modifiedConstitution = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.modifiedConstitution = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_intelligence(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.modifiedIntelligence = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.modifiedIntelligence = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_wisdom(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.modifiedWisdom = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.modifiedWisdom = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_charisma(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.modifiedCharisma = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.modifiedCharisma = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_base_strength(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.baseStrength = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.baseStrength = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_base_exc_strength(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.baseExceptionalStrength = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.baseExceptionalStrength = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_base_dexterity(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.baseDexterity = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.baseDexterity = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_base_constitution(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.baseConstitution = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.baseConstitution = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_base_intelligence(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.baseIntelligence = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.baseIntelligence = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_base_wisdom(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.baseWisdom = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.baseWisdom = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_base_charisma(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.baseCharisma = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.baseCharisma = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    

    void EobData::set_hit_points(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.hitPoints = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.hitPoints = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_max_hit_points(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.maxHitPoints = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.maxHitPoints = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_armor_class(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.armorClass = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.armorClass = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_class_id(const size_t characterIndex, const Class value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.classId = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.classId = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_race_sex(const size_t characterIndex, const Race raceId, const Sex sexId)
    {
        uint8_t value{};

        if (raceId == Race::human)
        {
            value = (sexId == Sex::male) ? 0 : 1;
        }
        else if (raceId == Race::elf)
        {
            value = (sexId == Sex::male) ? 2 : 3;
        }
        else if (raceId == Race::half_elf)
        {
            value = (sexId == Sex::male) ? 4 : 5;
        }
        else if (raceId == Race::dwarf)
        {
            value = (sexId == Sex::male) ? 6 : 7;
        }
        else if (raceId == Race::gnome)
        {
            value = (sexId == Sex::male) ? 8 : 9;
        }
        else if (raceId == Race::halfling)
        {
            value = (sexId == Sex::male) ? 10 : 11;
        }

        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.race = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.race = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_alignment(const size_t characterIndex, const Alignment value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.alignment = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.alignment = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_food_percentage(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.foodPerc = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.foodPerc = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_portrait(const size_t characterIndex, const short value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.portrait = static_cast<uint8_t>(value);
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.portrait = static_cast<uint8_t>(value);
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_level(const size_t characterIndex, const short value)
    {
        if (is_triple_class(characterIndex))
        {
            set_level(characterIndex, std::make_tuple(value, value, value));
        }
        else if (is_double_class(characterIndex))
        {
            set_level(characterIndex, std::make_tuple(value, value, std::nullopt));
        }
        else
        {
            set_level(characterIndex, std::make_tuple(value, std::nullopt, std::nullopt));
        }
    }

    void EobData::set_level(const size_t characterIndex, const Level value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            chr.level = std::get<0>(value);

            auto lev2 = std::get<1>(value).value_or(0);
            if (chr.levelMultiClass1 != 0 && lev2 != 0)
            {
                chr.levelMultiClass1 = lev2;
            }
            auto lev3 = std::get<2>(value).value_or(0);
            if (chr.levelMultiClass2 != 0 && lev3 != 0)
            {
                chr.levelMultiClass2 = lev3;
            }
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            chr.level = std::get<0>(value);

            auto lev2 = std::get<1>(value).value_or(0);
            if (chr.levelMultiClass1 != 0 && lev2 != 0)
            {
                chr.levelMultiClass1 = lev2;
            }
            auto lev3 = std::get<2>(value).value_or(0);
            if (chr.levelMultiClass2 != 0 && lev3 != 0)
            {
                chr.levelMultiClass2 = lev3;
            }
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }
    
    void EobData::set_experience(const size_t characterIndex, const uint32_t value)
    {
        if (is_triple_class(characterIndex))
        {
            set_experience(characterIndex, std::make_tuple(value, value, value));
        }
        else if (is_double_class(characterIndex))
        {
            set_experience(characterIndex, std::make_tuple(value, value, std::nullopt));
        }
        else
        {
            set_experience(characterIndex, std::make_tuple(value, std::nullopt, std::nullopt));
        }
    }
    
    void EobData::set_experience(const size_t characterIndex, const XP value)
    {
        if (_gameVersion == Version::eotb)
        {
            auto& chr = get_character<EotbData>(characterIndex);
            auto bytes1 = xps_to_bytes(std::get<0>(value));
            auto bytes2 = xps_to_bytes(std::get<1>(value).value_or(0));
            auto bytes3 = xps_to_bytes(std::get<2>(value).value_or(0));
            for (size_t sz=0; sz<4; ++sz)
            {
                chr.experience[sz] = bytes1[sz];
                if (chr.experienceMultiClass1[sz] != 0 && bytes2[sz] != 0)
                {
                    chr.experienceMultiClass1[sz] = bytes2[sz];
                }
                if (chr.experienceMultiClass2[sz] != 0 && bytes3[sz] != 0)
                {
                    chr.experienceMultiClass2[sz] = bytes3[sz];
                }
            }
        }
        else if (_gameVersion == Version::darkmoon)
        {
            auto& chr = get_character<DarkmoonData>(characterIndex);
            auto bytes1 = xps_to_bytes(std::get<0>(value));
            auto bytes2 = xps_to_bytes(std::get<1>(value).value_or(0));
            auto bytes3 = xps_to_bytes(std::get<2>(value).value_or(0));
            for (size_t sz=0; sz<4; ++sz)
            {
                chr.experience[sz] = bytes1[sz];
                if (chr.experienceMultiClass1[sz] != 0 && bytes2[sz] != 0)
                {
                    chr.experienceMultiClass1[sz] = bytes2[sz];
                }
                if (chr.experienceMultiClass2[sz] != 0 && bytes3[sz] != 0)
                {
                    chr.experienceMultiClass2[sz] = bytes3[sz];
                }
            }
        }
        else
        {
            throw std::runtime_error("Unhandled game version");
        }
    }

} // namespace EobLib
