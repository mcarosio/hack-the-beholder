#ifndef EOB_DATA_PRINTER_H
#define EOB_DATA_PRINTER_H

#include <eoblib.h>

namespace EobLib
{
    class EobDataPrinter
    {
    public:
        EobDataPrinter(const EobLib::EobData& eobData);

        void show_characters() const;
        void show_character(const size_t charIndex) const;

    private:
        const EobLib::EobData& _eobData;

        std::string format_strength(const size_t chrIndex) const;
        std::string format_base_strength(const size_t chrIndex) const;
        std::string format_skills(const size_t chrIndex) const;
        std::string format_base_skills(const size_t chrIndex) const;
        std::string format_xps(const size_t chrIndex) const;
    };
} // namespace EobLib

#endif //EOB_DATA_PRINTER_H