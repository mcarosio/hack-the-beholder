#ifndef EOBLIB_H
#define EOBLIB_H

#include <string>
#include <experimental/filesystem>
#include <fstream>
#include <vector>
#include <map>
#include <set>
#include <optional>

#include <config.h>
#include <eob_defs.h>

namespace EobLib
{
    class EobData
    {
    public:

        EobData(const std::experimental::filesystem::path& filePath, const Version gameVersion);

        std::string get_full_path() const;
        bool parse_file();
        void save_file(const std::experimental::filesystem::path& filePath);
        auto get_save_name() const -> std::string;
        void set_save_name(const std::string& saveName);

        auto is_active(const size_t characterIndex) const -> bool;
        auto get_name(const size_t characterIndex) const -> std::string;
        auto has_exceptional_strength(const size_t characterIndex) const -> bool;
        auto is_double_class(const size_t characterIndex) const -> bool;
        auto is_triple_class(const size_t characterIndex) const -> bool;

        void set_active(const size_t characterIndex, const bool active);
        void set_name(const size_t characterIndex, const std::string& chrName);

        auto get_skill(const Skill& skl, const size_t characterIndex) const -> short;
        auto get_strength(const size_t characterIndex) const -> short;
        auto get_exc_strength(const size_t characterIndex) const -> short;
        auto get_dexterity(const size_t characterIndex) const -> short;
        auto get_constitution(const size_t characterIndex) const -> short;
        auto get_intelligence(const size_t characterIndex) const -> short;
        auto get_wisdom(const size_t characterIndex) const -> short;
        auto get_charisma(const size_t characterIndex) const -> short;
        auto get_base_strength(const size_t characterIndex) const -> short;
        auto get_base_exc_strength(const size_t characterIndex) const -> short;
        auto get_base_dexterity(const size_t characterIndex) const -> short;
        auto get_base_constitution(const size_t characterIndex) const -> short;
        auto get_base_intelligence(const size_t characterIndex) const -> short;
        auto get_base_wisdom(const size_t characterIndex) const -> short;
        auto get_base_charisma(const size_t characterIndex) const -> short;

        auto get_hit_points(const size_t characterIndex) const -> short;
        auto get_max_hit_points(const size_t characterIndex) const -> short;
        auto get_armor_class(const size_t characterIndex) const -> short;
        auto get_class_id(const size_t characterIndex) const -> Class;
        auto get_race(const size_t characterIndex) const -> Race;
        auto get_sex(const size_t characterIndex) const -> Sex;
        auto get_alignment(const size_t characterIndex) const -> Alignment;
        auto get_food_percentage(const size_t characterIndex) const -> short;
        auto get_portrait(const size_t characterIndex) const -> short;

        auto get_level(const size_t characterIndex) const -> Level;
        auto get_experience(const size_t characterIndex) const -> XP;

        static const std::string_view get_class_description(const Class classId, const std::optional<std::string_view>& defaultValue = std::nullopt);
        static const std::string_view get_sex_description(const Sex sexId, const std::optional<std::string_view>& defaultValue = std::nullopt);
        static const std::string_view get_race_description(const Race raceId, const std::optional<std::string_view>& defaultValue = std::nullopt);
        static const std::string_view get_alignment_description(const Alignment alignId, const std::optional<std::string_view>& defaultValue = std::nullopt);
        static const std::string_view get_alignment_acronym(const Alignment alignId, const std::optional<std::string_view>& defaultValue = std::nullopt);
        static const std::string_view get_class_acronym(const Class classId, const std::optional<std::string_view>& defaultValue = std::nullopt);
        static const std::string_view get_skill_description(const Skill skill, const std::optional<std::string_view>& defaultValue = std::nullopt);

        void set_strength(const size_t characterIndex, const short value);
        void set_exc_strength(const size_t characterIndex, const short value);
        void set_dexterity(const size_t characterIndex, const short value);
        void set_constitution(const size_t characterIndex, const short value);
        void set_intelligence(const size_t characterIndex, const short value);
        void set_wisdom(const size_t characterIndex, const short value);
        void set_charisma(const size_t characterIndex, const short value);
        void set_base_strength(const size_t characterIndex, const short value);
        void set_base_exc_strength(const size_t characterIndex, const short value);
        void set_base_dexterity(const size_t characterIndex, const short value);
        void set_base_constitution(const size_t characterIndex, const short value);
        void set_base_intelligence(const size_t characterIndex, const short value);
        void set_base_wisdom(const size_t characterIndex, const short value);
        void set_base_charisma(const size_t characterIndex, const short value);

        void set_hit_points(const size_t characterIndex, const short value);
        void set_max_hit_points(const size_t characterIndex, const short value);
        void set_armor_class(const size_t characterIndex, const short value);
        void set_class_id(const size_t characterIndex, const Class value);
        void set_race_sex(const size_t characterIndex, const Race raceId, const Sex sexId);
        void set_alignment(const size_t characterIndex, const Alignment value);
        void set_food_percentage(const size_t characterIndex, const short value);
        void set_portrait(const size_t characterIndex, const short value);

        void set_level(const size_t characterIndex, const short value);
        void set_level(const size_t characterIndex, const Level value);
        void set_experience(const size_t characterIndex, const uint32_t value);
        void set_experience(const size_t characterIndex, const XP value);

    private:
        struct EotbData
        {
            uint8_t     characterId; //Character number (0-5).
            uint8_t     active; //Active character flag (0 - Inactive, 1 - Active).
            char        name[11]; //Character name, null terminating, max 10 characters.
            uint8_t     modifiedStrength; //Current strength value with buffs or curses.
            uint8_t     baseStrength; //Character's unmodified strength.
            uint8_t     modifiedExceptionalStrength; //Current exceptional strength value with buffs or curses.
            uint8_t     baseExceptionalStrength; //Character's unmodified exceptional strength.
            uint8_t     modifiedIntelligence; //Current intelligence value with buffs or curses.
            uint8_t     baseIntelligence; //Character's unmodified intelligence.
            uint8_t     modifiedWisdom; //Current wisdom value with buffs or curses.
            uint8_t     baseWisdom; //Character's unmodified wisdom.
            uint8_t     modifiedDexterity; //Current dexterity value with buffs or curses.
            uint8_t     baseDexterity; //Character's unmodified dexterity.
            uint8_t     modifiedConstitution; //Current constitution value with buffs or curses.
            uint8_t     baseConstitution; //Character's unmodified constitution.
            uint8_t     modifiedCharisma; //Current charisma value with buffs or curses.
            uint8_t     baseCharisma; //Character's unmodified charisma.
            uint8_t     hitPoints; //Character's current hit points.
            uint8_t     maxHitPoints; //Character's maximum hit points.
            int8_t      armorClass; //Signed for negative values. Gets recalculated as you change armor.
            uint8_t     unknown1; //Unknown. 
            uint8_t     race; //0 - Human Male, 1 - Human Female, 2 - Elf Male, 3 - Elf Female, 4 - Half-Elf Male, 5 - Half Elf Female, 6 - Dwarf Male, 7 - Dwarf Female, 8 - Gnome Male, 9 - Gnome Female, A - Halfling Male, B - Halfling Female
            uint8_t     classId; //0 - Fighter, 1 - Ranger, 2 - Paladin, 3 - Mage, 4 - Cleric, 5 - Thief, 6 - Fighter/Cleric, 7 - Fighter/Thief, 8 - Fighter/Mage, 9 - Fighter/Mage/Thief, A - Thief/Mage, B - Cleric/Thief, C - Fighter/Cleric/Mage, D - Ranger/Cleric, E - Cleric/Mage
            uint8_t     alignment; //0 - Lawful Good, 1 - Neutral Good, 2 - Chaotic Good, 3 - Lawful Neutral, 4 - True Neutral, 5 - Chaotic Neutral, 6 - Lawful Evil, 7 - Neutral Evil, 8 - Chaotic Evil
            uint8_t     portrait; //Character's artwork.
            uint8_t     foodPerc; //Percentage of satiated hunger (00-64).
            uint8_t     level; //Character level.
            uint8_t     levelMultiClass1; //Character level for multi-class (like fighter/thief).
            uint8_t     levelMultiClass2; //Character level for multi-class (like fighter/thief/mage).
            uint8_t     experience[4]; //Character's experience.
            uint8_t     experienceMultiClass1[4]; //Character's experience for multi-class.
            uint8_t     experienceMultiClass2[4]; //Character's experience for multi-class.
        };

        struct DarkmoonData
        {
            uint8_t     characterId; //Character number (0-5).
            uint8_t     active; //Active character flag (0 - Inactive, 1 - Active).
            char        name[11]; //Character name, null terminating, max 10 characters.
            uint8_t     modifiedStrength; //Current strength value with buffs or curses.
            uint8_t     baseStrength; //Character's unmodified strength.
            uint8_t     modifiedExceptionalStrength; //Current exceptional strength value with buffs or curses.
            uint8_t     baseExceptionalStrength; //Character's unmodified exceptional strength.
            uint8_t     modifiedIntelligence; //Current intelligence value with buffs or curses.
            uint8_t     baseIntelligence; //Character's unmodified intelligence.
            uint8_t     modifiedWisdom; //Current wisdom value with buffs or curses.
            uint8_t     baseWisdom; //Character's unmodified wisdom.
            uint8_t     modifiedDexterity; //Current dexterity value with buffs or curses.
            uint8_t     baseDexterity; //Character's unmodified dexterity.
            uint8_t     modifiedConstitution; //Current constitution value with buffs or curses.
            uint8_t     baseConstitution; //Character's unmodified constitution.
            uint8_t     modifiedCharisma; //Current charisma value with buffs or curses.
            uint8_t     baseCharisma; //Character's unmodified charisma.
            uint8_t     hitPoints; //Character's current hit points.
            uint8_t     dummy1;
            uint8_t     maxHitPoints; //Character's maximum hit points.
            uint8_t     dummy2;
            int8_t      armorClass; //Signed for negative values. Gets recalculated as you change armor.
            uint8_t     dummy3;
            uint8_t     race; //0 - Human Male, 1 - Human Female, 2 - Elf Male, 3 - Elf Female, 4 - Half-Elf Male, 5 - Half Elf Female, 6 - Dwarf Male, 7 - Dwarf Female, 8 - Gnome Male, 9 - Gnome Female, A - Halfling Male, B - Halfling Female
            uint8_t     classId; //0 - Fighter, 1 - Ranger, 2 - Paladin, 3 - Mage, 4 - Cleric, 5 - Thief, 6 - Fighter/Cleric, 7 - Fighter/Thief, 8 - Fighter/Mage, 9 - Fighter/Mage/Thief, A - Thief/Mage, B - Cleric/Thief, C - Fighter/Cleric/Mage, D - Ranger/Cleric, E - Cleric/Mage
            uint8_t     alignment; //0 - Lawful Good, 1 - Neutral Good, 2 - Chaotic Good, 3 - Lawful Neutral, 4 - True Neutral, 5 - Chaotic Neutral, 6 - Lawful Evil, 7 - Neutral Evil, 8 - Chaotic Evil
            uint8_t     portrait; //Character's artwork.
            uint8_t     foodPerc; //Percentage of satiated hunger (00-64).
            uint8_t     level; //Character level.
            uint8_t     levelMultiClass1; //Character level for multi-class (like fighter/thief).
            uint8_t     levelMultiClass2; //Character level for multi-class (like fighter/thief/mage).
            uint8_t     experience[4]; //Character's experience.
            uint8_t     experienceMultiClass1[4]; //Character's experience for multi-class.
            uint8_t     experienceMultiClass2[4]; //Character's experience for multi-class.
        };

        static auto read_xps(const uint8_t bytes[]) -> uint32_t;
        static auto xps_to_bytes(const uint32_t xps) -> std::vector<uint8_t>;
        auto is_valid_index(const size_t characterIndex) const -> bool;

        template <class _CharDataType>
        _CharDataType& get_character(const size_t characterIndex) const
        {
            if (characterIndex > Const::max_party_size-1)
            {
                throw std::runtime_error("Invalid character index");
            }
            auto chrSize{0};
            if (_gameVersion == Version::eotb)
            {
                chrSize = Const::character_data_length_eotb;
            }
            else if (_gameVersion == Version::darkmoon)
            {
                chrSize = Const::character_data_length_darkmoon;
            }

            auto head = _rawData.data() + (chrSize * characterIndex) + _headerLength;
            auto chr = (_CharDataType*)head;
            return *chr;
        }

        std::experimental::filesystem::path _saveFile;
        std::ifstream                       _inStream;
        std::vector<uint8_t>                _rawData;
        size_t                              _fileSize;
        Version                             _gameVersion;
        size_t                              _headerLength;

        static std::map<Class, std::string_view>         _classDescr;
        static std::map<Class, std::string_view>         _classAcronym;
        static std::map<Race, std::string_view>          _raceDescr;
        static std::map<Sex, std::string_view>           _sexDescr;
        static std::map<Alignment, std::string_view>     _alignDescr;
        static std::map<Alignment, std::string_view>     _alignAcronym;
        static std::map<Skill, std::string_view>         _skillDescr;
        
        static std::set<Class>                      _fighterClasses;
        static std::set<Class>                      _multiclasses;
        static std::set<Class>                      _doubleClass;
        static std::set<Class>                      _tripleClass;
    };

} // namespace EobLib

#endif //EOBLIB_H