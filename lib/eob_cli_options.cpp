#include "eob_cli_options.h"

namespace EobLib
{
const char* CliOptions::help = "help";
const char* CliOptions::path = "path";
const char* CliOptions::character = "character";
const char* CliOptions::version = "version";
const char* CliOptions::show = "show";
const char* CliOptions::set_strength = "set-strength";
const char* CliOptions::set_dexterity = "set-dexterity";
const char* CliOptions::set_constitution = "set-constitution";
const char* CliOptions::set_intelligence = "set-intelligence";
const char* CliOptions::set_wisdom = "set-wisdom";
const char* CliOptions::set_charisma = "set-charisma";
const char* CliOptions::set_hitpoints = "set-hitpoints";
const char* CliOptions::set_max_hitpoints = "set-max-hitpoints";
const char* CliOptions::set_experience = "set-experience";
const char* CliOptions::set_level = "set-level";
const char* CliOptions::set_alignment = "set-alignment";
const char* CliOptions::set_race = "set-race";
const char* CliOptions::set_sex = "set-sex";
const char* CliOptions::set_ac = "set-ac";
const char* CliOptions::set_class_id = "set-class-id";
const char* CliOptions::set_food_percentage = "set-food-percentage";
const char* CliOptions::set_portrait = "set-portrait";


EobCliOptions::EobCliOptions(int argc, char** argv)
    : _desc{"Allowed options"}, _vm{}
{
    add_options();

    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, _desc), _vm);
    boost::program_options::notify(_vm);  
}

void EobCliOptions::add_options()
{
    _desc.add_options()
        (CliOptions::help, "Hack the Beholder CLI tool online help")
        ("show", "show party data")
        ("path,P",
            boost::program_options::value<std::string>(),
            "set path for EOB save file. This option is required"
        )
        ("version,V",
            boost::program_options::value<std::string>(),
            "set EOB save file version ('e' for EOB, 'd': DARKMOON). This option is required"
        )
        ("character,C",
            boost::program_options::value<short>()->default_value(-1),
            "set character index (from 0 to 5). If not specified ALL characters will be affected"
        )
        ("set-strength,s",
            boost::program_options::value<std::string>(),
            "set strength value for the specified character index"
        )
        ("set-dexterity,d",
            boost::program_options::value<std::string>(),
            "set dexterity value for the specified character index"
        )
        ("set-constitution,c",
            boost::program_options::value<std::string>(),
            "set constitution value for the specified character index"
        )
        ("set-intelligence,i",
            boost::program_options::value<std::string>(),
            "set intelligence value for the specified character index"
        )
        ("set-wisdom,w",
            boost::program_options::value<std::string>(),
            "set wisdom value for the specified character index"
        )
        ("set-charisma,a",
            boost::program_options::value<std::string>(),
            "set charisma value for the specified character index"
        )
        ("set-hitpoints,h",
            boost::program_options::value<short>(),
            "set hit points value for the specified character index"
        )
        ("set-max-hitpoints,m",
            boost::program_options::value<short>(),
            "set maximum hit points value for the specified character index"
        )
        ("set-experience,x",
            boost::program_options::value<std::string>(),
            "set experience values for the specified character index"
        )
        ("set-level,l",
            boost::program_options::value<std::string>(),
            "set level values for the specified character index"
        )
        ("set-alignment,g",
            boost::program_options::value<std::string>(),
            "set moral aignment for the specified character index. "
            "Possible values are: LG = Legal Good, LN = Legal Neutral, LE = Legal Evil, "
            "NG = Neutral Good, N = True Neutral, NE = Neutral Evil, "
            "CG = Chaotic Good, CN = Chaotic Neutral, CE = Chaotic Evil"
        )
        ("set-race,r",
            boost::program_options::value<std::string>(),
            "set race for the specified character index. "
            "Possible values are: h = Human, d = Dwarf, e = Elf, he = Half Elf, hl = Halfling, g = Gnome"
        )
        ("set-sex",
            boost::program_options::value<char>(),
            "set sex for the specified character index. "
            "Possible values are: m = male, f = female"
        )
        ("set-ac",
            boost::program_options::value<short>(),
            "set AC for the specified character index"
        )
        ("set-class-id",
            boost::program_options::value<short>(),
            "set class ID for the specified character index"
        )
        ("set-food-percentage,f",
            boost::program_options::value<short>(),
            "set food percentage value for the specified character index"
        )
        ("set-portrait,t",
            boost::program_options::value<short>(),
            "set portrait for the specified character index"
        )
        ;
}

} // namespace EobLib