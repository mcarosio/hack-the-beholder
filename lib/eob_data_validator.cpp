#include <eob_data_validator.h>

namespace EobLib
{

EobDataValidator::EobDataValidator()
{
}

std::optional<std::pair<short, std::optional<short>>> EobDataValidator::validate_strength(const std::string& stringValue) const
{
    std::regex pattern("(2[0-5]|1[0-9]|[1-9])", std::regex_constants::ECMAScript);
    std::regex patternExc("(18)/([0-9][0-9])", std::regex_constants::ECMAScript);
    
    std::smatch matches;
    short strengthValue{0};
    short excStrengthValue{0};

    if (std::regex_match(stringValue, matches, pattern))
    {
        strengthValue = std::stoi(matches[1]);
    }
    else if (std::regex_match(stringValue, matches, patternExc))
    {
        strengthValue = std::stoi(matches[1]);
        if (strengthValue == 18)
        {
            excStrengthValue = std::stoi(matches[2]);
            excStrengthValue = (excStrengthValue == 0) ? 100 : excStrengthValue;
        }
    }
    else
    {
        return std::nullopt;
    }

    return std::make_pair(strengthValue, excStrengthValue);
}

std::optional<short> EobDataValidator::validate_skill(const std::string& stringValue) const
{
    std::regex pattern("(2[0-5]|1[0-9]|[1-9])", std::regex_constants::ECMAScript);
    
    std::smatch matches;
    short skillValue{0};

    if (std::regex_match(stringValue, matches, pattern))
    {
        skillValue = std::stoi(matches[1]);
    }
    else
    {
        return std::nullopt;
    }

    return skillValue;
}

std::optional<EobLib::Version> EobDataValidator::validate_version(const std::string& stringValue) const
{
    if (stringValue == "e")
    {
        return EobLib::Version::eotb;
    }
    else if (stringValue == "d")
    {
        return EobLib::Version::darkmoon;
    }
    return std::nullopt;
}

std::optional<EobLib::Race> EobDataValidator::validate_race(const std::string& stringValue) const
{
    if (stringValue == "h")
    {
        return EobLib::Race::human;
    }
    else if (stringValue == "e")
    {
        return EobLib::Race::elf;
    }
    else if (stringValue == "d")
    {
        return EobLib::Race::dwarf;
    }
    else if (stringValue == "he")
    {
        return EobLib::Race::half_elf;
    }
    else if (stringValue == "hl")
    {
        return EobLib::Race::halfling;
    }
    return std::nullopt;
}

std::optional<EobLib::Sex> EobDataValidator::validate_sex(const char& stringValue) const
{
    if (stringValue == 'm')
    {
        return EobLib::Sex::male;
    }
    else if (stringValue == 'f')
    {
        return EobLib::Sex::female;
    }
    return std::nullopt;
}

std::optional<EobLib::Alignment> EobDataValidator::validate_alignment(const std::string& stringValue) const
{
    if (stringValue == EobLib::Description::AlignAcronym::lawful_good)
    {
        return EobLib::Alignment::lawful_good;
    }
    else if (stringValue == EobLib::Description::AlignAcronym::lawful_neutral)
    {
        return EobLib::Alignment::lawful_neutral;
    }
    else if (stringValue == EobLib::Description::AlignAcronym::lawful_evil)
    {
        return EobLib::Alignment::lawful_evil;
    }
    else if (stringValue == EobLib::Description::AlignAcronym::neutral_good)
    {
        return EobLib::Alignment::neutral_good;
    }
    else if (stringValue == EobLib::Description::AlignAcronym::neutral)
    {
        return EobLib::Alignment::true_neutral;
    }
    else if (stringValue == EobLib::Description::AlignAcronym::neutral_evil)
    {
        return EobLib::Alignment::neutral_evil;
    }
    else if (stringValue == EobLib::Description::AlignAcronym::chaotic_good)
    {
        return EobLib::Alignment::chaotic_good;
    }
    else if (stringValue == EobLib::Description::AlignAcronym::chaotic_neutral)
    {
        return EobLib::Alignment::chaotic_neutral;
    }
    else if (stringValue == EobLib::Description::AlignAcronym::chaotic_evil)
    {
        return EobLib::Alignment::chaotic_evil;
    }
    return std::nullopt;
}

std::optional<EobLib::Class> EobDataValidator::validate_class_id(const std::string& stringValue) const
{
    if (stringValue == EobLib::Description::ClassAcronym::fighter)
    {
        return EobLib::Class::fighter;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::ranger)
    {
        return EobLib::Class::ranger;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::paladin)
    {
        return EobLib::Class::paladin;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::mage)
    {
        return EobLib::Class::mage;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::cleric)
    {
        return EobLib::Class::cleric;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::thief)
    {
        return EobLib::Class::thief;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::fighter_cleric)
    {
        return EobLib::Class::fighter_cleric;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::fighter_thief)
    {
        return EobLib::Class::fighter_thief;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::fighter_mage)
    {
        return EobLib::Class::fighter_mage;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::fighter_mage_thief)
    {
        return EobLib::Class::fighter_mage_thief;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::thief_mage)
    {
        return EobLib::Class::thief_mage;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::cleric_thief)
    {
        return EobLib::Class::cleric_thief;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::fighter_cleric_mage)
    {
        return EobLib::Class::fighter_cleric_mage;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::ranger_cleric)
    {
        return EobLib::Class::ranger_cleric;
    }
    else if (stringValue == EobLib::Description::ClassAcronym::cleric_mage)
    {
        return EobLib::Class::cleric_mage;
    }
    return std::nullopt;
}

} // namespace EobLib