#include "eob_data_printer.h"
#include <iomanip>
#include <iostream>
#include <sstream>

namespace EobLib
{

EobDataPrinter::EobDataPrinter(const EobLib::EobData& eobData)
    : _eobData{eobData}
{
}

void EobDataPrinter::show_characters() const
{
    for (size_t c=0; c<EobLib::Const::max_party_size; ++c)
    {
        show_character(c);
    }
}

void EobDataPrinter::show_character(const size_t charIndex) const
{
    bool isActive = _eobData.is_active(charIndex);
    std::stringstream ss;
    ss << _eobData.get_name(charIndex) << " (" << ((isActive) ? "Active" : "Inactive") << ")\n";

    if (isActive)
    {
        ss << EobLib::EobData::get_race_description(_eobData.get_race(charIndex)) << " ";
        ss << EobLib::EobData::get_sex_description(_eobData.get_sex(charIndex)) << " ";

        ss << EobLib::EobData::get_class_description(_eobData.get_class_id(charIndex));
        ss << " (" << EobLib::EobData::get_alignment_description(_eobData.get_alignment(charIndex)) << ")\n";
        
        ss << "\t" << format_skills(charIndex) << "\n";
        ss << "\t" << format_base_skills(charIndex) << "\n";

        ss << "\tAC: " << _eobData.get_armor_class(charIndex) << "\n";
        ss << "\tHP: " << _eobData.get_hit_points(charIndex) << "/" << _eobData.get_max_hit_points(charIndex) << "\n";
        ss << "\tFood: " << _eobData.get_food_percentage(charIndex) << "\n";

        ss << format_xps(charIndex) << "\n";
    }
    
    std::cout << ss.str();
}

std::string EobDataPrinter::format_strength(const size_t chrIndex) const
{
    std::stringstream val{};
    val << _eobData.get_strength(chrIndex);
    if (_eobData.has_exceptional_strength(chrIndex))
    {
        auto exc = _eobData.get_exc_strength(chrIndex);
        if (exc == 100)
        {
            val << "/00";
        }
        else
        {
            val << "/" << std::setw(2) << std::setfill('0') << exc;
        }
    }
    return val.str();
}

std::string EobDataPrinter::format_base_strength(const size_t chrIndex) const
{
    std::stringstream val{};
    val << _eobData.get_base_strength(chrIndex);
    if (_eobData.has_exceptional_strength(chrIndex))
    {
        auto exc = _eobData.get_base_exc_strength(chrIndex);
        if (exc == 100)
        {
            val << "/00";
        }
        else
        {
            val << "/" << std::setw(2) << std::setfill('0') << exc;
        }
    }
    return val.str();
}

std::string EobDataPrinter::format_skills(const size_t chrIndex) const
{
    std::stringstream skills{};
    skills << "Skills:"
        << " STR: " << format_strength(chrIndex)
        << " DEX: " << _eobData.get_dexterity(chrIndex)
        << " CON: " << _eobData.get_constitution(chrIndex)
        << " INT: " << _eobData.get_intelligence(chrIndex)
        << " WIS: " << _eobData.get_wisdom(chrIndex)
        << " CHA: " << _eobData.get_charisma(chrIndex);
    return skills.str();
}

std::string EobDataPrinter::format_base_skills(const size_t chrIndex) const
{
    std::stringstream skills{};
    skills << "Skills:"
        << " STR: " << format_base_strength(chrIndex)
        << " DEX: " << _eobData.get_base_dexterity(chrIndex)
        << " CON: " << _eobData.get_base_constitution(chrIndex)
        << " INT: " << _eobData.get_base_intelligence(chrIndex)
        << " WIS: " << _eobData.get_base_wisdom(chrIndex)
        << " CHA: " << _eobData.get_base_charisma(chrIndex);
    return skills.str();
}

std::string EobDataPrinter::format_xps(const size_t chrIndex) const
{
    std::stringstream xps{};
    auto clsId = _eobData.get_class_id(chrIndex);
    xps << "\tExperience:\n\t\t";
    xps << _eobData.get_class_description(clsId) << ": ";
    auto lvl = _eobData.get_level(chrIndex);

    xps << std::get<0>(lvl);
    auto l2 = std::get<1>(lvl);
    if (l2.has_value())
    {
        xps << "/" << l2.value();
    }
    auto l3 = std::get<2>(lvl);
    if (l3.has_value())
    {
        xps << "/" << l3.value();
    }

    xps << "\n\t\tXP: ";
    auto pts = _eobData.get_experience(chrIndex);

    xps << std::get<0>(pts);
    auto p2 = std::get<1>(pts);
    if (p2.has_value())
    {
        xps << "/" << p2.value();
    }
    auto p3 = std::get<2>(pts);
    if (p3.has_value())
    {
        xps << "/" << p3.value();
    }
    return xps.str();
}

} // namespace EobLib