#ifndef EOB_DEFS_H
#define EOB_DEFS_H

#include <optional>
#include <tuple>

namespace EobLib
{
    enum class Version
    {
        eotb,
        darkmoon
    };

    enum class Race
    {
        human,
        elf,
        half_elf,
        dwarf,
        gnome,
        halfling
    };

    enum class Sex
    {
        male,
        female
    };

    enum class Class
    {
        fighter,
        ranger,
        paladin,
        mage,
        cleric,
        thief,
        fighter_cleric,
        fighter_thief,
        fighter_mage,
        fighter_mage_thief,
        thief_mage,
        cleric_thief,
        fighter_cleric_mage,
        ranger_cleric,
        cleric_mage
    };

    enum class Alignment
    {
        lawful_good,
        neutral_good,
        chaotic_good,
        lawful_neutral,
        true_neutral,
        chaotic_neutral,
        lawful_evil,
        neutral_evil,
        chaotic_evil
    };

    enum class Skill
    {
        strength,
        dexterity,
        constitution,
        intelligence,
        wisdom,
        charisma
    };

    using Level = std::tuple<short, std::optional<short>, std::optional<short>>;
    using XP = std::tuple<uint32_t, std::optional<uint32_t>, std::optional<uint32_t>>;

    struct Const
    {
        static constexpr size_t darkmoon_header_length{20};
        static constexpr size_t character_data_length_eotb{243};
        static constexpr size_t character_data_length_darkmoon{345};
        static constexpr short max_party_size{6};
    };

    namespace Description
    {
        struct Version
        {
            static constexpr std::string_view eotb = "EotB";
            static constexpr std::string_view darkmoon = "Darkmoon";
        };
        
        struct Race
        {
            static constexpr std::string_view human = "Human";
            static constexpr std::string_view elf = "Elf";
            static constexpr std::string_view half_elf = "Half Elf";
            static constexpr std::string_view dwarf = "Dwarf";
            static constexpr std::string_view gnome = "Gnome";
            static constexpr std::string_view halfling = "Halfling";
        };
        
        struct Sex
        {
            static constexpr std::string_view male = "Male";
            static constexpr std::string_view female = "Female";
        };
        
        struct Class
        {
            static constexpr std::string_view fighter = "Fighter";
            static constexpr std::string_view ranger = "Ranger";
            static constexpr std::string_view paladin = "Paladin";
            static constexpr std::string_view mage = "Mage";
            static constexpr std::string_view cleric = "Cleric";
            static constexpr std::string_view thief = "Thief";
            static constexpr std::string_view fighter_cleric = "Fighter/Cleric";
            static constexpr std::string_view fighter_thief = "Fighter/Thief";
            static constexpr std::string_view fighter_mage = "Fighter/Mage";
            static constexpr std::string_view fighter_mage_thief = "Fighter/Mage/Thief";
            static constexpr std::string_view thief_mage = "Thief/Mage";
            static constexpr std::string_view cleric_thief = "Cleric/Thief";
            static constexpr std::string_view fighter_cleric_mage = "Fighter/Cleric/Mage";
            static constexpr std::string_view ranger_cleric = "Ranger/Cleric";
            static constexpr std::string_view cleric_mage = "Cleric/Mage";
        };
        
        struct ClassAcronym
        {
            static constexpr std::string_view fighter = "F";
            static constexpr std::string_view ranger = "R";
            static constexpr std::string_view paladin = "P";
            static constexpr std::string_view mage = "M";
            static constexpr std::string_view cleric = "C";
            static constexpr std::string_view thief = "T";
            static constexpr std::string_view fighter_cleric = "F/C";
            static constexpr std::string_view fighter_thief = "F/T";
            static constexpr std::string_view fighter_mage = "F/M";
            static constexpr std::string_view fighter_mage_thief = "F/M/T";
            static constexpr std::string_view thief_mage = "T/M";
            static constexpr std::string_view cleric_thief = "C/T";
            static constexpr std::string_view fighter_cleric_mage = "F/C/M";
            static constexpr std::string_view ranger_cleric = "R/C";
            static constexpr std::string_view cleric_mage = "C/M";
        };
        
        struct Alignment
        {
            static constexpr std::string_view lawful_good = "Lawful Good";
            static constexpr std::string_view neutral_good = "Neutral Good";
            static constexpr std::string_view chaotic_good = "Chaotic Good";
            static constexpr std::string_view lawful_neutral = "Lawful Neutral";
            static constexpr std::string_view neutral = "True Neutral";
            static constexpr std::string_view chaotic_neutral = "Chaotic Neutral";
            static constexpr std::string_view lawful_evil = "Lawful Evil";
            static constexpr std::string_view neutral_evil = "Neutral Evil";
            static constexpr std::string_view chaotic_evil = "Chaotic Evil";
        };
        
        struct AlignAcronym
        {
            static constexpr std::string_view lawful_good = "LG";
            static constexpr std::string_view neutral_good = "NG";
            static constexpr std::string_view chaotic_good = "CG";
            static constexpr std::string_view lawful_neutral = "LN";
            static constexpr std::string_view neutral = "N";
            static constexpr std::string_view chaotic_neutral = "CN";
            static constexpr std::string_view lawful_evil = "LE";
            static constexpr std::string_view neutral_evil = "NE";
            static constexpr std::string_view chaotic_evil = "CE";
        };
        
        struct Skill
        {
            static constexpr std::string_view strength = "Strength";
            static constexpr std::string_view dexterity = "Dexterity";
            static constexpr std::string_view constitution = "Constitution";
            static constexpr std::string_view intelligence = "Intelligence";
            static constexpr std::string_view wisdom = "Wisdom";
            static constexpr std::string_view charisma = "Charisma";
        };
    };
    
} // namespace EobLib

#endif //EOB_DEFS_H