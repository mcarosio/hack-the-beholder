#include <iostream>
#include <filesystem>
#include <string>
#include <optional>
#include <regex>

#include <eoblib.h>
#include <eob_data_printer.h>
#include <eob_data_validator.h>
#include <eob_cli_options.h>

std::string_view get_eob_version_description(const EobLib::Version& version);
void run(EobLib::EobData& d, const EobLib::EobCliOptions& opts, EobLib::EobDataValidator& validator, EobLib::EobDataPrinter& printer, const size_t charIndex);

int main(int argc, char** argv)
{
    std::cout << "Hack the Beholder CLI tool\n\n";
    EobLib::EobCliOptions cliOpts{argc, argv};

    if (cliOpts.has_option(EobLib::CliOptions::help)) {
        std::cout << cliOpts.get_help() << "\n";
        return 0;
    }

    if (!cliOpts.has_option(EobLib::CliOptions::path))
    {
        std::cerr << "Input file not specified.\n";
        return 1;
    }

    if (!cliOpts.has_option(EobLib::CliOptions::version))
    {
        std::cerr << "File version not specified.\n";
        return 1;
    }

    EobLib::EobDataValidator validator{};
    auto eobVersion = cliOpts.get_option<std::string>(EobLib::CliOptions::version, "").value();
    auto vers = validator.validate_version(eobVersion);
    if (!vers.has_value())
    {
        std::cerr << "Invalid file version specified.\n";
        return 1;
    }

    auto fullPath = cliOpts.get_option<std::string>(EobLib::CliOptions::path, "").value();
    std::experimental::filesystem::path filePath{fullPath};

    EobLib::EobData eobData{filePath, vers.value()};
    EobLib::EobDataPrinter printer{eobData};

    if (!eobData.parse_file())
    {
        std::cerr << "Unable to parse file " << eobData.get_full_path() << "\n";
        return 1;
    }
    
    std::cout << "Input file: " << eobData.get_full_path() << "\n";
    std::cout << "Game version: " << get_eob_version_description(vers.value()) << "\n";

    size_t filterCharIndex{0};
    if (cliOpts.has_option(EobLib::CliOptions::character))
    {
        filterCharIndex = cliOpts.get_option<short>(EobLib::CliOptions::character, 0).value();
    }
    
    for (size_t cIdx=1; cIdx<=EobLib::Const::max_party_size; ++cIdx)
    {
        if (filterCharIndex == 0 || cIdx == filterCharIndex)
        {
            run(eobData, cliOpts, validator, printer, cIdx);
            if (eobData.is_active(cIdx-1) && cliOpts.has_option(EobLib::CliOptions::show))
            {
                std::cout << "Showing saved data\n";
                printer.show_character(cIdx-1);
            }
        }
    }

    eobData.save_file(filePath);

    return 0;
}

void run(EobLib::EobData& d, const EobLib::EobCliOptions& opts, EobLib::EobDataValidator& validator, EobLib::EobDataPrinter& printer, const size_t cIdx)
{
    size_t charIndex = cIdx-1;
    if (opts.has_option(EobLib::CliOptions::set_strength))
    {
        auto val = opts.get_option<std::string>(EobLib::CliOptions::set_strength).value();
        auto skillName = EobLib::EobData::get_skill_description(EobLib::Skill::strength);
        std::cout << "Changing " << skillName << " value to " << val << "\n";
        
        auto strVals = validator.validate_strength(val);
        if (strVals.has_value())
        {
            short val = strVals.value().first;
            d.set_strength(charIndex, val);
            d.set_base_strength(charIndex, val);

            short excValue = strVals.value().second.value_or(0);
            if (d.has_exceptional_strength(charIndex) && val == 18 && excValue == 0)
            {
                excValue = 1;
            }
            d.set_exc_strength(charIndex, excValue);
            d.set_base_exc_strength(charIndex, excValue);
        }
        else
        {
            std::cerr << "Invalid " << skillName << " specified\n";
        }
    }
    if (opts.has_option(EobLib::CliOptions::set_dexterity))
    {
        auto val = opts.get_option<std::string>(EobLib::CliOptions::set_dexterity).value();
        auto skillName = EobLib::EobData::get_skill_description(EobLib::Skill::dexterity);
        std::cout << "Changing " << skillName << " value to " << val << "\n";
        
        auto sklVals = validator.validate_skill(val);
        if (sklVals.has_value())
        {
            d.set_dexterity(charIndex, sklVals.value());
            d.set_base_dexterity(charIndex, sklVals.value());
        }
        else
        {
            std::cerr << "Invalid " << skillName << " specified\n";
        }
    }
    if (opts.has_option(EobLib::CliOptions::set_constitution))
    {
        auto val = opts.get_option<std::string>(EobLib::CliOptions::set_constitution).value();
        auto skillName = EobLib::EobData::get_skill_description(EobLib::Skill::constitution);
        std::cout << "Changing " << skillName << " value to " << val << "\n";
        
        auto sklVals = validator.validate_skill(val);
        if (sklVals.has_value())
        {
            d.set_constitution(charIndex, sklVals.value());
            d.set_base_constitution(charIndex, sklVals.value());
        }
        else
        {
            std::cerr << "Invalid " << skillName << " specified\n";
        }
    }
    if (opts.has_option(EobLib::CliOptions::set_intelligence))
    {
        auto val = opts.get_option<std::string>(EobLib::CliOptions::set_intelligence).value();
        auto skillName = EobLib::EobData::get_skill_description(EobLib::Skill::intelligence);
        std::cout << "Changing " << skillName << " value to " << val << "\n";
        
        auto sklVals = validator.validate_skill(val);
        if (sklVals.has_value())
        {
            d.set_intelligence(charIndex, sklVals.value());
            d.set_base_intelligence(charIndex, sklVals.value());
        }
        else
        {
            std::cerr << "Invalid " << skillName << " specified\n";
        }
    }
    if (opts.has_option(EobLib::CliOptions::set_wisdom))
    {
        auto val = opts.get_option<std::string>(EobLib::CliOptions::set_wisdom).value();
        auto skillName = EobLib::EobData::get_skill_description(EobLib::Skill::wisdom);
        std::cout << "Changing " << skillName << " value to " << val << "\n";
        
        auto sklVals = validator.validate_skill(val);
        if (sklVals.has_value())
        {
            d.set_wisdom(charIndex, sklVals.value());
            d.set_base_wisdom(charIndex, sklVals.value());
        }
        else
        {
            std::cerr << "Invalid " << skillName << " specified\n";
        }
    }
    if (opts.has_option(EobLib::CliOptions::set_charisma))
    {
        auto val = opts.get_option<std::string>(EobLib::CliOptions::set_charisma).value();
        auto skillName = EobLib::EobData::get_skill_description(EobLib::Skill::charisma);
        std::cout << "Changing " << skillName << " value to " << val << "\n";
        
        auto sklVals = validator.validate_skill(val);
        if (sklVals.has_value())
        {
            d.set_charisma(charIndex, sklVals.value());
            d.set_base_charisma(charIndex, sklVals.value());
        }
        else
        {
            std::cerr << "Invalid " << skillName << " specified\n";
        }
    }
    if (opts.has_option(EobLib::CliOptions::set_hitpoints))
    {
        auto val = opts.get_option<short>(EobLib::CliOptions::set_hitpoints).value();
        std::cout << "Changing HP value to " << val << "\n";
        d.set_hit_points(charIndex, val);
    }
    if (opts.has_option(EobLib::CliOptions::set_max_hitpoints))
    {
        auto val = opts.get_option<short>(EobLib::CliOptions::set_max_hitpoints).value();
        std::cout << "Changing max HP value to " << val << "\n";
        d.set_max_hit_points(charIndex, val);
    }
    if (opts.has_option(EobLib::CliOptions::set_experience))
    {
        auto val = opts.get_option<std::string>(EobLib::CliOptions::set_experience).value();
        std::cout << "Changing XP value to " << val << "\n";
        
        auto values = validator.validate_three_values<uint32_t>(val);
        if (values.has_value())
        {
            auto val1 = std::get<0>(values.value());
            auto val2 = std::get<1>(values.value());
            auto val3 = std::get<2>(values.value());
            EobLib::XP vals = std::make_tuple(val1, val2, val3);
            d.set_experience(charIndex, vals);
        }
        else
        {
            std::cerr << "Invalid xp specified\n";
        }
    }
    if (opts.has_option(EobLib::CliOptions::set_level))
    {
        auto val = opts.get_option<std::string>(EobLib::CliOptions::set_level).value();
        std::cout << "Changing level value to " << val << "\n";
        
        auto values = validator.validate_three_values<short>(val);
        if (values.has_value())
        {
            auto val1 = std::get<0>(values.value());
            auto val2 = std::get<1>(values.value());
            auto val3 = std::get<2>(values.value());
            EobLib::Level vals = std::make_tuple(val1, val2, val3);
            d.set_level(charIndex, vals);
        }
        else
        {
            std::cerr << "Invalid strength specified\n";
        }
    }
    if (opts.has_option(EobLib::CliOptions::set_alignment))
    {
        auto val = opts.get_option<std::string>(EobLib::CliOptions::set_alignment).value();
        auto align = validator.validate_alignment(val);
        if (!align)
        {
            std::cerr << "Invalid alignmant specified\n";
        }
        else
        {
            auto alignDesc = EobLib::EobData::get_alignment_description(align.value(), "Unknown");
            std::cout << "Changing alignment value to " << alignDesc << "\n";
            d.set_alignment(charIndex, align.value());
        }
    }
    if (opts.has_option(EobLib::CliOptions::set_race))
    {
        auto val = opts.get_option<std::string>(EobLib::CliOptions::set_race).value();
        auto race = validator.validate_race(val);
        if (!race)
        {
            std::cerr << "Invalid race specified\n";
        }
        else
        {
            auto raceDesc = EobLib::EobData::get_race_description(race.value(), "Unknown");
            std::cout << "Changing race value to " << raceDesc << "\n";
            auto sex = d.get_sex(charIndex);
            d.set_race_sex(charIndex, race.value(), sex);
        }
    }
    if (opts.has_option(EobLib::CliOptions::set_sex))
    {
        auto val = opts.get_option<char>(EobLib::CliOptions::set_sex).value();
        auto sex = validator.validate_sex(val);
        if (!sex)
        {
            std::cerr << "Invalid sex specified\n";
        }
        else
        {
            auto sexDesc = EobLib::EobData::get_sex_description(sex.value(), "Unknown");
            std::cout << "Changing sex value to " << sexDesc << "\n";
            auto race = d.get_race(charIndex);
            d.set_race_sex(charIndex, race, sex.value());
        }
    }
    if (opts.has_option(EobLib::CliOptions::set_ac))
    {
        auto val = opts.get_option<short>(EobLib::CliOptions::set_ac).value();
        std::cout << "Changing AC value to " << val << "\n";
        d.set_armor_class(charIndex, val);
    }
    if (opts.has_option(EobLib::CliOptions::set_class_id))
    {
        auto val = opts.get_option<std::string>(EobLib::CliOptions::set_class_id).value();
        auto cls = validator.validate_class_id(val);
        if (cls.has_value())
        {
            std::cout << "Changing Class value to " << EobLib::EobData::get_class_description(cls.value()) << "\n";
            d.set_class_id(charIndex, cls.value());
        }
        else
        {
            std::cerr << "Invalid class specified\n";
        }
    }
    if (opts.has_option(EobLib::CliOptions::set_food_percentage))
    {
        auto val = opts.get_option<short>(EobLib::CliOptions::set_food_percentage).value();
        std::cout << "Changing foot percentage value to " << val << "\n";
        d.set_food_percentage(charIndex, val);
    }
    if (opts.has_option(EobLib::CliOptions::set_portrait))
    {
        auto val = opts.get_option<short>(EobLib::CliOptions::set_portrait).value();
        std::cout << "Changing portrait value to " << val << "\n";
        d.set_portrait(charIndex, val);
    }
}

std::string_view get_eob_version_description(const EobLib::Version& version)
{
    if (version == EobLib::Version::eotb)
    {
        return EobLib::Description::Version::eotb;
    }
    else if (version == EobLib::Version::darkmoon)
    {
        return EobLib::Description::Version::darkmoon;
    }
    return "Unknown";
}