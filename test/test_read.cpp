#include <catch2/catch_all.hpp>
#include <string>
#include <cstdint>

#include <eoblib.h>

using namespace EobLib;

static std::string fileName = "darkmoon-qsp.sav";
static std::string fullPath = EobLibSettings::data_dir_path + fileName;
static std::experimental::filesystem::path filePath{fullPath};

static Version eobFileVersion = Version::darkmoon;

TEST_CASE("[TC-READ.001] Read Darkmoon heroes data", "[read, darkmoon]" )
{
	std::map<size_t, std::tuple<std::string, EobLib::Race, EobLib::Sex, EobLib::Class, EobLib::Alignment>> data
	{
		{0, {"PERICLES", Race::human, Sex::male, Class::paladin, Alignment::lawful_good}},
		{1, {"\"STUMPY\"", Race::dwarf, Sex::male, Class::fighter_thief, Alignment::true_neutral}},
		{2, {"WOLFSPIRIT", Race::half_elf, Sex::male, Class::cleric, Alignment::lawful_good}},
		{3, {"LAURANN", Race::elf, Sex::female, Class::mage, Alignment::chaotic_good}},
	};

    EobData darkmoonData{filePath, eobFileVersion};
    REQUIRE(darkmoonData.parse_file());

	for (auto d : data)
	{
		const size_t heroId{d.first};
		const std::string heroName{std::get<0>(d.second)};
		const EobLib::Race heroRace{std::get<1>(d.second)};
		const EobLib::Sex heroSex{std::get<2>(d.second)};
		const EobLib::Class heroClass{std::get<3>(d.second)};
		const EobLib::Alignment heroAlignment{std::get<4>(d.second)};

		REQUIRE(darkmoonData.is_active(heroId));
		REQUIRE(darkmoonData.get_name(heroId) == heroName);
		REQUIRE(darkmoonData.get_race(heroId) == heroRace);
		REQUIRE(darkmoonData.get_sex(heroId) == heroSex);
		REQUIRE(darkmoonData.get_class_id(heroId) == heroClass);
		REQUIRE(darkmoonData.get_alignment(heroId) == heroAlignment);
	}
}

TEST_CASE("[TC-READ.002] Read Darkmoon heroes skills", "[read, darkmoon]" )
{
    const std::map<size_t, std::map<EobLib::Skill, short>> heroSkills
    {
        {0, {{Skill::strength, 18}, {Skill::dexterity, 15}, {Skill::constitution, 17}, {Skill::intelligence, 9}, {Skill::wisdom, 16}, {Skill::charisma, 17}}},
        {1, {{Skill::strength, 18}, {Skill::dexterity, 17}, {Skill::constitution, 19}, {Skill::intelligence, 10}, {Skill::wisdom, 6}, {Skill::charisma, 3}}},
        {2, {{Skill::strength, 15}, {Skill::dexterity, 16}, {Skill::constitution, 16}, {Skill::intelligence, 11}, {Skill::wisdom, 18}, {Skill::charisma, 13}}},
        {3, {{Skill::strength, 12}, {Skill::dexterity, 19}, {Skill::constitution, 11}, {Skill::intelligence, 18}, {Skill::wisdom, 10}, {Skill::charisma, 15}}},
    };
	const std::map<size_t, short> excStrength{{0, 100}, {1, 78}, {2, 0}, {3, 0},};
	
    EobData darkmoonData{filePath, eobFileVersion};
    REQUIRE(darkmoonData.parse_file());

	for (auto h : heroSkills)
	{
		for (auto skl : h.second)
		{
			auto sklValue = darkmoonData.get_skill(skl.first, h.first);
			REQUIRE(sklValue == skl.second);
		}
	}
}

TEST_CASE("[TC-READ.003] Read Darkmoon heroes health data", "[read, darkmoon]" )
{
	std::map<size_t, std::tuple<short, short, short, short, EobLib::Level, EobLib::XP>> data
	{
		{0, {1, 78, 78, 99, {6, std::nullopt, std::nullopt}, {69000, std::nullopt, std::nullopt}}},
		{1, {0, 78, 78, 99, {6, 6, std::nullopt}, {34500, 34500, std::nullopt}}},
		{2, {1, 70, 70, 99, {7, std::nullopt, std::nullopt}, {69000, std::nullopt, std::nullopt}}},
		{3, {6, 28, 28, 99, {7, std::nullopt, std::nullopt}, {69000, std::nullopt, std::nullopt}}},
	};

    EobData darkmoonData{filePath, eobFileVersion};
    REQUIRE(darkmoonData.parse_file());

	for (auto d : data)
	{
		REQUIRE(darkmoonData.get_armor_class(d.first) == std::get<0>(d.second));
		REQUIRE(darkmoonData.get_hit_points(d.first) == std::get<1>(d.second));
		REQUIRE(darkmoonData.get_max_hit_points(d.first) == std::get<2>(d.second));
    	REQUIRE(darkmoonData.get_food_percentage(d.first) == std::get<3>(d.second));
    	REQUIRE(darkmoonData.get_level(d.first) == std::get<4>(d.second));
    	REQUIRE(darkmoonData.get_experience(d.first) == std::get<5>(d.second));
	}
}