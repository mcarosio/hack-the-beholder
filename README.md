![Logo](images/eob-intro.png)

Hack the Beholder - Eye of the Beholder hacking tool
=====================================================================

Written by Marco Carosio [https://marcocarosio.it/](https://marcocarosio.it/)

> DISCLAIMER: This software is unofficial, not approved or endorsed by right-owner company. It is released as-is and I assumes no responsibility or liability for any errors or damages it may cause or improper use of the tool.

Hack-the-Beholder is released under [MIT License](LICENSE.md)
* it's free of charge;
* it doesn't make use of copyrighted material;
* it's not sponsored by third parties.

Hack-the-Beholder is an open C++ library. It aims to inspect and modify the content of [Eye of the Beholder](https://en.wikipedia.org/wiki/Eye_of_the_Beholder_(video_game)) and [Eye of the Beholder II](https://en.wikipedia.org/wiki/Eye_of_the_Beholder_II:_The_Legend_of_Darkmoon) save game files.
The tool allows for arbitrary changes of the file contents, so be cautious before a ligh-hearted editing. Do a backup before of the files before start editing...

## Getting Started
The easiest way to have the library compiled, along with the other components built upon it, is typing
<pre>
$ git clone https://gitlab.com/mcarosio/hack-the-beholder.git
$ cd hack-the-beholder
$ mkdir -p build/
$ cd build/
$ cmake .. [-DRUN_UNIT_TESTS=on|off]
$ cmake --build . -j16
</pre>
After that, the test suite will be executed (it may take time). Moreover, tests failure will compromise the correct build of the test executable. If you want to make changes or you would like to perform some debugging on tests, it's better to avoid running the test suite at the end of the build process.
Tests can be skipped specifying the command line option -RUN_UNIT_TESTS=off as in the example
<pre>
$ mkdir -p build/
$ cd build/
$ cmake .. -DRUN_UNIT_TESTS=off
</pre>

## Dependencies
No dependencies for the moment...

## CLI tool
The library implements most of the desired features which can be easily accessed via the official command line tool for ease of access to the binary information inside the EOBDATA save files.
The CLI tool is ablt to handle both Eye of the Beholder I and II file formats, just type
<pre>
% ./build/cli/htb_cli --help
</pre>
for help. The following are a few example of common use cases. A sample input file is made available in the sample_data subdirectory. The file is taken from the Quick Save Party available with Eye of the Beholder II: Legend of Darkmoon.

## Display character's data
Version selected: '-V d' for Darkmoon.
Character index: '-C 1' refers to the first character in the party.
The selected character's data is shown.
<pre>
$ htb_cli -P sample_data/darkmoon-qsp.sav -V d -C 0 --show
</pre>

## Change character's experience
Version selected: '-V d' for Darkmoon.
Character index: '-C 1' refers to the first character in the party.
XP value for the selected character is set to 100000, then data is shown.
<pre>
$ htb_cli -P sample_data/darkmoon-qsp.sav -V d -C 0 -x 100000 --show
</pre>

## Change character's strength value
Version selected: '-V d' for Darkmoon.
Character index: '-C 2' refers to the second character in the party.
Strength value for the selected character is set to 18/90, then data is shown.
<pre>
$ htb_cli -P ../sample_data/darkmoon-qsp.sav -V d -C 2 --show -s 18/90
</pre>
If the selected character is not entitled to exceptional strength (i.e. is not a warrior), then 18 will be applied as a value.

## Change character's XP value
Version selected: '-V d' for Darkmoon.
Character index: '-C 2' refers to the second character in the party.
XP value for the selected character is set to 50000/50000, then data is shown.
<pre>
$ htb_cli -P ../sample_data/darkmoon-qsp.sav -V d -C 2 --show -x 50000/50000
</pre>
If the selected character is not multiclass, then only the first value is applied.

If the selected character is multiclass and the XP values specified does not equal the number of classes, then the missing value is not modified. In the following example, "Stumpy" (6th level fighter/thief) will only receive 50000 XP as a fighter, while his XP as a thief are untouched.
<pre>
$ htb_cli -P ../sample_data/darkmoon-qsp.sav -V d -C 2 --show -x 50000
</pre>

## Change character's level
Version selected: '-V d' for Darkmoon.
Character index: '-C 2' refers to the second character in the party.
Level for the selected character is set to 8/8, then data is shown.
<pre>
$ htb_cli -P ../sample_data/darkmoon-qsp.sav -V d -C 2 --show -l 7/7
</pre>
If the selected character is not multiclass, then only the first value is applied.

If the selected character is multiclass and the level specified does not equal the number of classes, then the missing value is not modified. In the following example, "Stumpy" (6th level fighter/thief) will only reach 7th level as a fighter, while his level as a thief is untouched.
<pre>
$ htb_cli -P ../sample_data/darkmoon-qsp.sav -V d -C 2 --show -l 7
</pre>

Have fun and long life to Eye of the Beholder saga!